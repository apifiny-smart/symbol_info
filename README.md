# symbol_info

This repo holds security master information such as tick size across different exchanges, and the format is customized for [Apifiny Algo](https://algo.apifiny.com/) which introduces a new way to trade cryptocurrencies and their derivatives! It is like an operating system for trading and allows you to trade on many exchanges easily and flexibly.

